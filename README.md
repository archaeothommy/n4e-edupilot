# NFDI4Earth Educational Pilot "Teaching lead isotope geochemistry and application in archaeometry (LIGA-A)" submission materials

This repository contains the educational materials created within the framework of the NFDI4Earth Educational Pilot "Teaching lead isotope geochemistry and application in archaeometry (LIGA-A)". They are embedded in the still ongoing development of "Lead isotopes in Archaeology: an interactive textbook", which is part of the [GlobaLID project](https://archmetaldbm.github.io/Globalid/). They represent chapters 3, 4, 5, and 20 in the current version of this textbook. 

The set of materials contain a collection of different interactive elements created with H5P and the learning units in which they are used. In addition, an expert interview and a timelapse were produced and can be accessed [in this folder](https://hessenbox-a10.rz.uni-frankfurt.de/getlink/fi4BNqH1J6ZKQ47N5KwHMB/). 

All materials are license under a Creative [Commons Attribution International 4.0 license](https://creativecommons.org/licenses/by/4.0/), making them available for use, distribution and reuse.

## Interactive materials

All interactive materials are provided in an editable format (`h5p`) and an exported standalone HTML. You can edit h5p files either [online](https://h5p.org/) or with the free editor [Lumi](https://app.lumi.education/). 

### Ion Exchange Chromatography (IEC)
A puzzle that requires to reorder the steps of an ion-exchange protocol for the separation of lead into the correct order. By actively figuring out the different steps of the protocol (by trial and error or closely watching the timelapse video) a more intimate knowledge of the protocol will be gained than by just reading the steps. 

### MC-ICP-MS scheme
A hotspot figure with the general layout of a multi-collector mass spectrometer with inductively coupled plasma and photos of the different components. This material allows learners to get a glimpse into the "bowels" of such instruments. Most labs do not have such instruments and if they do, students rarely have access to them. Moreover, the components are usually hidden behind the instrument’s hull. 

### Ore_minerals
A hotspot figure showing a schematical vein mineralisation of copper ore. It provides information about the interaction between the environment (esp. meteoric water) and the mineralisation, i.e. its weathering. In addition, typical ore minerals are shown as it is expected that not all learners are familiar with them from their studies. 

### Sample_digestion
A small serious game that provides training on which equipment and acid combinations can be used to dissolve typical samples for lead isotope analysis in mineralogy and archaeometry. 

## Learning units
Each learning unit is provided in quarto-flavoured markdown (`qmd`) and the corresponding HTML-version. 

The topics of the four learning units are: 
* Basics of lead isotope geochemistry
* Preparing a sample for the measurement of the lead isotope composition
* Basics of mass spectrometry and the measurement of lead isotopes
* Visualisation of lead isotope data in archaeometry

They are designed as self-paced learning units in the style of an textbook enhanced with the interactive materials developed in the project. All units follow the same outline: 

* Learning objective -- the goal of the unit
* Prior knowledge -- the expected already existing knowledge
* Material -- which type of materials will be used in this unit
* Learning content -- the learning content
* Self check -- Question for the learner to self-check their progress
* Further reading -- additional media and literature to deepen the knowledge about the unit's topic

They all target students on the master level (in Geoscience) but are designed to be suitable to learners with a wide range of backgrounds as long as they have a basic understanding of sciences, especially chemistry and physics. 

## Curriculum
The full outline of the textbook (in German) is included because it is the base for a full curriculum on this topic, combining aspects from geosciences/geochemistry, archaeology and data science/research data management. 

## Acknowledgements 
This work has been partially funded by the German Research Foundation (DFG) through the project NFDI4Earth (NFDI4Earth Educational Pilot, DFG project no. 460036893, <https://www.nfdi4earth.de/>) within the German National Research Data Infrastructure (NFDI, <https://www.nfdi.de/>). 
